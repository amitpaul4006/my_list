import express, { Application, Request, Response } from 'express';
import bodyParser from 'body-parser';
import myListRoutes from './routes/myList.routes';
import mongoose from 'mongoose';
const PORT = process.env.PORT || 3000;


const MONGO_URI = "mongodb+srv://amitpaul4006:q9jsZG3tHfYtQFem@cluster0.n76pupa.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";

const app: express.Application = express(); // Initialize app with express()

let mongooseConnection: mongoose.Mongoose;

if (process.env.TEST_MODE === 'true') {
    beforeAll(async () => {
        mongooseConnection = await mongoose.connect(MONGO_URI);
        console.log('MongoDB connected successfully');
    });

    afterAll(async () => {
        await mongooseConnection.disconnect();
    });
}


// Configure middleware
// Apply middleware
app.use(bodyParser.json());
// No need for bodyParser.urlencoded for JSON body parsing
app.use(bodyParser.urlencoded({ extended: true })); // Optional, can be removed if not needed

// Handle potential parsing errors
app.use((err: Error, req: Request, res: Response, next: Function) => {
    if (err instanceof SyntaxError) {
        res.status(400).json({ message: 'Invalid JSON body' });
    } else {
        next(err); // Pass other errors to default error handler
    }
});

// Define routes
app.use('/api/my-list', myListRoutes); // isAuthenticated , Apply authentication for my-list routes

// Connect to MongoDB using Mongoose with error handling
mongoose.connect(MONGO_URI)
    .then(() => console.log('MongoDB connected successfully'))
    .catch((error) => console.error('Error connecting to MongoDB:', error));

// **Start the server with dynamic port and error handling**
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
})
    .on('error', (err) => {
        console.error('Server error:', err);
    });

export default app;