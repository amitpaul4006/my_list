module.exports = {
    testTimeout: 30000,
    transform: {
        '^.+\\.jsx?$': 'babel-jest',
    },

    testMatch: [
        "**/__tests__/**/*.+(ts|tsx|js|jsx)",
        "**/?(*.)+(spec|test).+(ts|tsx|js|jsx)"
    ],
    moduleFileExtensions: [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    preset: 'ts-jest',
    testEnvironment: 'node',
    roots: ['<rootDir>/__test__'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],

};