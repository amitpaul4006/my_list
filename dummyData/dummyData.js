const mongoose = require('mongoose');

const dummyUser = {
    _id: new mongoose.Types.ObjectId('639e6a229477228b28b28b2b'),
    user_id: '12345',
    username: 'johndoe',
};

const dummyMovie = {
    _id: new mongoose.Types.ObjectId('639e6a239477228b28b28b2c'),
    title: 'The Shawshank Redemption',
};

const dummyTvShow = {
    _id: new mongoose.Types.ObjectId('639e6a249477228b28b28b2d'),
    title: 'Friends',
};

const existingListWithItems = {
    ...dummyUser,
    movies: [dummyMovie._id],
    tvShows: [dummyTvShow._id],
    total: 2,
};

const emptyList = {
    ...dummyUser,
    movies: [],
    tvShows: [],
    total: 0,
};

module.exports = {
    dummyUser,
    dummyMovie,
    dummyTvShow,
    existingListWithItems,
    emptyList,
};