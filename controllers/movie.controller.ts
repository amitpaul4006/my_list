// movie.controller.ts

import { Request, Response } from 'express';
import MovieModel from '../models/movie.model';

const addMovie = async (req: Request, res: Response) => {
    try {
        const { title, overview, poster_path, release_date, vote_average } = req.body;

        // Validate input data (optional)
        // You can add checks for required fields, data types, etc.

        const newMovie = new MovieModel({
            title,
            overview,
            poster_path,
            release_date,
            vote_average,
        });

        const savedMovie = await newMovie.save();

        return res.status(201).json({ message: 'Movie added successfully', movie: savedMovie });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error adding movie' });
    }
};

export default addMovie;
