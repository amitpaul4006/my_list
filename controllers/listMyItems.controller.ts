import { Request, Response } from 'express';
import MyListModel, { MyList } from '../models/myList.model';
import mongoose from 'mongoose';

const listMyItems = async (req: Request, res: Response) => {
    const { userId, listId } = req.query;

    try {
        // Validate user ID or list ID (optional)
        if (!userId && !listId) {
            return res.status(400).json({ message: 'Missing required parameter: userId or listId' });
        }

        // Find MyList based on userId or listId
        const query = userId ? { user_id: userId } : { list_id: listId };
        const existingList = await MyListModel.findOne(query)
            .populate('movies') // Populate movies details
            .populate('tvShows'); // Populate TV shows details

        if (!existingList || (existingList.movies.length === 0 && existingList.tvShows.length === 0)) {
            return res.json({ items: [], total: 0 });
        }

        // Type casting with more specific type definition
        const destructuredList = existingList as MyList & {
            movies: mongoose.Schema.Types.ObjectId[];
            tvShows: mongoose.Schema.Types.ObjectId[];
            total: number;
        };

        const { movies, tvShows, username } = destructuredList;
        const items = [...movies, ...tvShows];
        const total = items.length;

        // Handle pagination (optional)
        // ... (code for pagination logic using page and limit from query if applicable)

        return res.json({ items, total, username }); // Include username
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error listing My List items' });
    }
};

export default listMyItems;