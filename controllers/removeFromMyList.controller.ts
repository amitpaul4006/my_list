import { Request, Response } from 'express';
import MyListModel from '../models/myList.model';
import mongoose from 'mongoose';

const removeFromMyList = async (req: Request, res: Response) => {
    const { userId, itemId } = req.params; // Assuming item ID comes from URL params

    try {
        const existingList = await MyListModel.findOne({ userId });

        if (!existingList) {
            return res.status(404).json({ message: 'List not found' });
        }

        if (!mongoose.Types.ObjectId.isValid(itemId)) {
            return res.status(400).json({ message: 'Invalid item ID' });
        }

        const convertedItemId = new mongoose.Types.ObjectId(itemId); // Create a new ObjectId instance

        const movieIndex = existingList.movies.findIndex((item) => convertedItemId.equals(new mongoose.Types.ObjectId(item.toString())));
        const tvShowIndex = existingList.tvShows.findIndex((item) => convertedItemId.equals(new mongoose.Types.ObjectId(item.toString())));
        if (movieIndex > -1) {
            existingList.movies.splice(movieIndex, 1);
        } else if (tvShowIndex > -1) {
            existingList.tvShows.splice(tvShowIndex, 1);
        } else {
            return res.status(404).json({ message: 'Item not found in list' });
        }

        await existingList.save();
        return res.json({ message: 'Item removed successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error removing from My List' });
    }
};

export default removeFromMyList;