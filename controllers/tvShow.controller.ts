// tvShow.controller.ts

import { Request, Response } from 'express';
import TVShowModel from '../models/tvShow.model';

const addTVShow = async (req: Request, res: Response) => {
    try {
        const { name, overview, poster_path, first_air_date, vote_average } = req.body;

        // Validate input data (optional)
        // You can add checks for required fields, data types, etc.

        const newTVShow = new TVShowModel({
            name,
            overview,
            poster_path,
            first_air_date,
            vote_average,
        });

        const savedTVShow = await newTVShow.save();

        return res.status(201).json({ message: 'TV Show added successfully', tvShow: savedTVShow });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error adding TV Show' });
    }
};

export default addTVShow;
