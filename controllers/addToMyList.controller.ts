import { Request, Response } from 'express';
import MyListModel, { MyList } from '../models/myList.model';
import mongoose from 'mongoose';

const addToMyList = async (req: Request, res: Response) => {
    const { user_id, username, movie_id, tv_show_id } = req.body;

    try {
        // Input validation (optional)
        if (!user_id || !username) {
            return res.status(400).json({ message: 'Missing required fields: user_id and username' });
        }

        let userObjectId: mongoose.Types.ObjectId;

        try {
            userObjectId = new mongoose.Types.ObjectId(user_id);
        } catch (error) {
            return res.status(400).json({ message: 'Invalid user ID' });
        }

        const existingList = await MyListModel.findOne({ user_id: userObjectId });

        // ... rest of the code (assuming it uses movie_id and/or tv_show_id)

    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error retrieving My List' });
    }
};

export default addToMyList;
