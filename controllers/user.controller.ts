import { Request, Response } from 'express';
import UserModel from '../models/user.model';

export const addUser = async (req: Request, res: Response) => {
    const { name, email } = req.body;

    try {
        const existingUser = await UserModel.findOne({ email });
        if (existingUser) {
            return res.status(400).json({ message: 'Email already exists' });
        }

        const newUser = new UserModel({ name, email });
        const savedUser = await newUser.save();

        return res.json(savedUser);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error adding user' });
    }
};

export const deleteUser = async (req: Request, res: Response) => {
    const { userId } = req.params;

    try {
        const deletedUser = await UserModel.findByIdAndDelete(userId);

        if (!deletedUser) {
            return res.status(404).json({ message: 'User not found' });
        }

        return res.json({ message: 'User deleted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error deleting user' });
    }
};

