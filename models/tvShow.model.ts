// tvShow.model.ts

import mongoose from 'mongoose';

interface TVShow {
    name: string;
    overview: string;
    poster_path: string;
    first_air_date: Date;
    vote_average: number;
    // Add more fields as needed (genres, cast, seasons, etc.)
}

const tvShowSchema = new mongoose.Schema<TVShow>({
    name: { type: String, required: true },
    overview: { type: String },
    poster_path: { type: String },
    first_air_date: { type: Date },
    vote_average: { type: Number },
    // Add more fields here
});

export default mongoose.model<TVShow>('TVShow', tvShowSchema);
