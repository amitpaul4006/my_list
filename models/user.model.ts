import mongoose, { Schema, Document } from 'mongoose';

export interface User extends Document {
    _id: mongoose.Types.ObjectId; // Auto-generated ID
    name: string;
    email: string;
    // Add more fields as needed (password, etc.)
}

const userSchema = new Schema<User>({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    // Add more fields here
});

export default mongoose.model<User>('User', userSchema);
