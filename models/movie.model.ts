// movie.model.ts

import mongoose from 'mongoose';

interface Movie {
    title: string;
    overview: string;
    poster_path: string;
    release_date: Date;
    vote_average: number;
    // Add more fields as needed (genres, cast, etc.)
}

const movieSchema = new mongoose.Schema<Movie>({
    title: { type: String, required: true },
    overview: { type: String },
    poster_path: { type: String },
    release_date: { type: Date },
    vote_average: { type: Number }
});

export default mongoose.model<Movie>('Movie', movieSchema);
