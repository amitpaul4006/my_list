import mongoose, { Schema, Document } from 'mongoose';

export interface MyList extends Document {
    user_id: string; // Required user ID
    list_id: string; // Auto-generated ID
    username: string; // Required username
    movies: mongoose.Schema.Types.ObjectId[];
    tvShows: mongoose.Schema.Types.ObjectId[];
}

const myListSchema = new Schema<MyList>({
    user_id: { type: String, required: true },
    list_id: { type: String, default: () => new mongoose.Types.ObjectId().toString() }, // Auto-generated ID
    username: { type: String, required: true },
    movies: [{ type: Schema.Types.ObjectId, ref: 'Movie' }],
    tvShows: [{ type: Schema.Types.ObjectId, ref: 'TVShow' }], // Reference TVShow model
});

const MyListModel = mongoose.model<MyList>('MyList', myListSchema);

export default MyListModel;
