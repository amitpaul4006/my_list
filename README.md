my_list - A Project for Managing Your Favorite Movies and TV Shows
This project provides a backend API for creating and managing a list of your favorite movies and TV shows.

Project Description
This Node.js application allows users to add, remove, and view their favorite movies and TV shows. It utilizes MongoDB as the database to store user data and media entries.

Setting Up and Running the Application
Prerequisites:

Node.js and npm (or yarn) installed on your system.
A MongoDB database instance running locally or on a cloud provider.


Steps:

1.Clone the repository:
git clone https://github.com/your-username/my_list.git

2.Install dependencies:
cd my_list
npm install

3.Configure MongoDB connection:

Create a .env file in the project root directory.

Add the following line, replacing <your_mongo_uri> with your actual MongoDB connection URI:

MONGO_URI: 
"mongodb+srv://amitpaul4006:q9jsZG3tHfYtQFem@cluster0.n76pupa.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";

4.Start the server:
npm start
This will start the server in development mode, listening on port 3000 by default.

5.Running Tests:
npm test
This command will execute all unit tests using Jest.

6.Design Choices and Assumptions : 

6.1. Express.js: Chosen as a lightweight and flexible Node.js framework for building the API.

6.2 Mongoose: Employed as an object data modeling (ODM) library for interacting with MongoDB.

6.3 RESTful API: Implemented for a consistent and predictable API design.

7.Assumptions:

7.1 The provided MongoDB connection URI is valid and allows write operations.

7.2 User authentication and authorization are not implemented in this basic version.

8.Routes: 

The application provides the following API routes:

8.1 POST /api/my-list/users - Creates a new user. (Not currently authenticated)

8.2 DELETE /api/my-list/users/:userId - Deletes a user by ID. (Not currently authenticated)

8.3 POST /api/my-list/add - Adds a movie or TV show to the user's list.

8.4 DELETE /api/my-list/remove/:id - Removes a movie or TV show from the user's list.

8.5 GET /api/my-list/list - Retrieves the user's list of movies and TV shows.

8.6 POST /api/my-list/movie - Adds a specific movie to the user's list.

8.7 POST /api/my-list/tvshow - Adds a specific TV show to the user's list.

9.Additional Notes

9.1 Refer to the models and controllers directories for details on data models and API logic.

9.2 The __tests__ directory contains unit tests for the controllers.

9.3 Consider implementing authentication and authorization mechanisms for production use.

This README file provides a starting point for understanding the my_list project. Feel free to explore the codebase further and contribute improvements!


