import { Request, Response } from 'express';
import removeFromMyList from '../controllers/removeFromMyList.controller';
const { dummyUser, dummyMovie, existingListWithItems } = require('../dummyData/dummyData');
import mongoose from 'mongoose';
import MyListModel from '../models/myList.model';

jest.mock('express');
describe('removeFromMyList controller', () => {
    let mongooseConnection: typeof mongoose;

    beforeAll(async () => {
        mongooseConnection = await mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/test');
    });

    afterAll(async () => {
        if (mongooseConnection) {
            await mongooseConnection.disconnect();
        }
    });

    it('should return 404 for list not found', async () => {
        const req: Request = { params: { userId: 'invalidUserId' } } as unknown as Request;
        const res: Response = {} as Response;
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn();

        await removeFromMyList(req, res);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).toHaveBeenCalledWith({ message: 'List not found' });
    }, 30000);

    it('should return 400 for invalid item ID', async () => {
        const req: Request = { params: { userId: dummyUser.user_id, itemId: 'invalidId' } } as unknown as Request;
        const res: Response = {} as Response;
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn();

        await removeFromMyList(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({ message: 'Invalid item ID' });
    }, 30000);

    // Add more tests for removing TV shows and error handling (e.g., item not found in list)
});
