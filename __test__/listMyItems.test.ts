import { Request, Response } from 'express';
import listMyItems from '../controllers/listMyItems.controller';
const { existingListWithItems, emptyList } = require('../dummyData/dummyData');
import MyListModel from '../models/myList.model';

jest.mock('express');

describe('listMyItems controller', () => {
    it('should return 400 for missing userId and listId', async () => {
        const req: Request = { query: {} } as Request; // Set an empty query object
        const res: Response = {} as Response;
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn();

        await listMyItems(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({ message: 'Missing required parameter: userId or listId' });
    }, 10000);
});