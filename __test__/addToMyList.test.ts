import { Request, Response } from 'express';
import addToMyList from '../controllers/addToMyList.controller';
const { dummyUser } = require('../dummyData/dummyData');

jest.mock('express');

describe('addToMyList controller', () => {
    it('should return 400 for missing required fields', async () => {
        const req: Request = { body: {} } as Request; // Set an empty body object
        const res: Response = {} as Response;
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn();

        await addToMyList(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({ message: 'Missing required fields: user_id and username' });
    }, 10000);

});
