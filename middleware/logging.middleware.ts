import { NextFunction, Request, Response } from 'express';

const loggingMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const { method, url, body } = req;
    console.log(`${method} ${url} - Body: ${JSON.stringify(body)}`);
    next();
};

export default loggingMiddleware;
