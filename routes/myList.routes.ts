import addToMyList from '../controllers/addToMyList.controller';
import listMyItems from '../controllers/listMyItems.controller';
import addMovie from '../controllers/movie.controller';
import removeFromMyList from '../controllers/removeFromMyList.controller';
import { addUser, deleteUser } from '../controllers/user.controller'


// routes/myList.routes.ts
import express, { Router } from 'express';
import addTVShow from '../controllers/tvShow.controller';

const router: Router = express.Router();

// POST - Add user
router.post('/users', addUser);

// DELETE - Delete user by ID
router.delete('/users/:userId', deleteUser);

// Add to My List
router.post('/add', addToMyList);

// Remove from My List
router.delete('/remove/:id', removeFromMyList);

// List My Items
router.get('/list', listMyItems);

// Add Movie 
router.post('/movie', addMovie);

// Add TV Show 
router.post('/tvshow', addTVShow);

export default router;